## init
建立空的 angular application
```shell
ng new app1 --create-application=false
```

建立 app1 application
```shell
ng g application app1 --routing
```

加入 module-federation 套件
```shell
ng add @angular-architects/module-federation --project app1 --port 3000
```

## 功能實作

加入 query module
```shell
ng g m query --routing
```

QueryModule加入 query component
```shell
ng g c query
```

webpack.config.js 匯出 QueryModule
<img src="./doc/webpack.config.png">
QueryModule 為自定義的名稱，框要引用的時候需要用該名稱對應


## 共用 library 資料
在 webpack.config.js 中 shared 加入要共用的 library，並設定 singleton: true
```shell
"package 名稱": { singleton: true }
```
- Singleton: 是否為單例，default 為 false
- strictVersion: 為 true 時，實際應用依賴的版本不一致時，會抛錯。
- requiredVersion: 指定共享依賴的版本，default 為當前的依賴版本
- eager: 在打包過程中是否被分離為 async chunk，為 true 時，會打包到 main、remoteEntry，不會被分離，因此設置為 true 時共享依賴是沒意義的。

## 透過 CustomEvent 傳遞資料
```shell
const data = {
  bubbles: true,
  detail: {
    eventType: 'test',
    customData: this.eventBusData
  }
};
const busEvent = new CustomEvent('app-event-bus', data);
dispatchEvent(busEvent);
```
new CustomEvent('app-event-bus', data)
- app-event-bus 為自定義的事件名稱
- data 需要交換的資料


## 透過 CustomEvent 接收資料
訂閱 CustomEvent 事件
```shell
fromEvent<CustomEvent>(window, 'app-event-bus').subscribe((e) => {
  // do something
});
```
fromEvent<CustomEvent>(window, 'app-event-bus')
- app-event-bus 為自定義的事件名稱
- e 即為丟出來的資料

