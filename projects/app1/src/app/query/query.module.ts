import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QueryRoutingModule } from './query-routing.module';
import { QueryComponent } from './query/query.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    QueryComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    QueryRoutingModule
  ]
})
export class QueryModule { }
