import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedLibService } from 'shared-lib';
@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.css']
})
export class QueryComponent implements OnInit {

  inputData = '';
  eventBusData = '';

  constructor(
    private shareLibService: SharedLibService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  nextData() {
    console.log('next => ', this.inputData);
    this.shareLibService.data.next(this.inputData);
  }

  redirectBase() {
    this.router.navigate(['/view']);
  }

  eventBusPostData() {
    const data = {
      bubbles: true,
      detail: {
        eventType: 'test',
        customData: this.eventBusData
      }
    };
    const busEvent = new CustomEvent('app-event-bus', data);
    dispatchEvent(busEvent);
  }
}
